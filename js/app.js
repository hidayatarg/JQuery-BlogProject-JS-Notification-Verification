                                                                            // Part-1


/*
// you can use jQuery in place of the $
// intially hide
$('#flashMessage').hide();

// fade in 2ms
//$('#flashMessage').fadeIn(2000);
$('#flashMessage').slideDown(2000);
$('#flashMessage').delay(3000);
$('#flashMessage').slideUp();

const title="My first Blog Post";
const content = "this is my <strong>first</strong>  post!!";

//selecting Id
$('#blogTitlePreview').text(title);
$('#blogContentPreview').html(content);
*/
$('#flashMessage').hide();

$('#previewButton').click(function(){
   const title= $('#blogTitleInput').val();
   const content = $('#blogContentInput').val();

    $('#blogTitlePreview').text(title);
    $('#blogContentPreview').html(content);

    //display the message when message is clicked
    $('#flashMessage').slideDown(2000)
    .delay(3000)
    .slideUp();
});
